#!/usr/bin/python

import os
import sys
import unittest
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from lib.sqlite_db import SqliteDB

class SqliteDBTest(unittest.TestCase):

    def setUp(self):
        os.environ['SQL_PATH'] = os.path.join(sys.path[0], '..', "sql")
        self.db = SqliteDB()

    def test_modify_article_data(self):
        article_data = (
            'test_title',   #title
            'This is content of test article.', #content
            'tag1,tag2',    #tags
            1234,           #article_id
            'raix',         #author_id
            12345678        #create_date
        )
        article_content = article_data[1]
        article_id = article_data[3]

        # Test insert article data
        self.db.store_article_data(article_data)
        self.assertTrue(self.db.exist_article_id(article_id))

        # Test get article data
        select = self.db.get_article_data(article_id)
        for i in range(len(article_data)):
            self.assertEqual(article_data[i], select[i])

        # Test get article content by article id
        select = self.db.get_articles_by("article_id", article_id)
        self.assertEqual(article_content, select[0][0])

        # Test delete article data
        self.db.delete_article_data(article_id)
        self.assertFalse(self.db.exist_article_id(article_id))

    def tearDown(self):
        self.db.close()

if __name__ == '__main__':
	unittest.main()
